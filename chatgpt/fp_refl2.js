class Complex {
    constructor(real, imag) {
      this.real = real;
      this.imag = imag;
    }
    add(other) {
      return new Complex(this.real + other.real, this.imag + other.imag);
    }
    sub(other) {
      return new Complex(this.real - other.real, this.imag - other.imag);
    }
    mul(other) {
      const real = this.real * other.real - this.imag * other.imag;
      const imag = this.real * other.imag + this.imag * other.real;
      return new Complex(real, imag);
    }
    div(other) {
      const denom = other.real ** 2 + other.imag ** 2;
      const real = (this.real * other.real + this.imag * other.imag) / denom;
      const imag = (this.imag * other.real - this.real * other.imag) / denom;
      return new Complex(real, imag);
    }
    abs() {
      return Math.sqrt(this.real ** 2 + this.imag ** 2);
    }
    arg() {
      return Math.atan2(this.imag, this.real);
    }
    exp() {
      const real = Math.exp(this.real) * Math.cos(this.imag);
      const imag = Math.exp(this.real) * Math.sin(this.imag);
      return new Complex(real, imag);
    }
}

const width = 1200;
const height = 800;
const gap = 10;
const margin = { top: 20, right: 20, bottom: 60, left: 80 };
const innerWidth = width - margin.left - margin.right;
const innerHeight = height/2 - gap - margin.top - margin.bottom;

const svg = d3.select('#plot')
  .attr("id", "mainsvg")
  .attr('width', width)
  .attr('height', height);

svg1 = d3.select("svg#mainsvg")
  .append("g") // group to move svg down
    .attr('transform', `translate(${margin.left},${margin.top})`)
    .append("svg")
    .attr("id", "svg1")
    .attr("height", innerHeight)
    .attr("width", innerWidth)
svg2 = d3.select("svg#mainsvg")
  .append("g") // group to move svg down
    .attr("transform", `translate(0, ${innerHeight+gap})`)
    .append("svg")
    .attr("id", "svg2")
    .attr("height", innerHeight)
    .attr("width", innerWidth)

const xScale = d3.scaleLinear()
  .domain([-Math.PI, Math.PI])
  .range([0, innerWidth]);

const yScale = d3.scaleLinear()
  .domain([0, 1])
  .range([innerHeight, 0]);

const xAxis = d3.axisBottom(xScale)
  .ticks(8)
  .tickSizeOuter(0);

const yAxis = d3.axisLeft(yScale)
  .ticks(5)
  .tickSizeOuter(0);

// Group for svg1
// const g = svg.append('g')
//   .attr('transform', `translate(${margin.left},${margin.top})`);
g = d3.select("svg#svg1")

g.append('g')
  .attr('class', 'x-axis')
  .attr('transform', `translate(0,${innerHeight})`)
  .call(xAxis);

g.append('g')
  .attr('class', 'y-axis')
  .call(yAxis);

g.append('path')
  .attr('class', 'line')
  .attr('fill', 'none')
  .attr('stroke', 'steelblue')
  .attr('stroke-width', 2);

g.selectAll('.tick text')
  .style('font-size', '20px');

// Add horizontal grid lines
g.append('g')
  .attr('class', 'grid')
  .call(d3.axisLeft(yScale)
          .tickSize(-innerWidth)
          .tickFormat(''))
  .selectAll('.tick line')
  .attr('stroke-opacity', 0.2)
  .attr('stroke-dasharray', '2,2');

// Add vertical grid lines
g.append('g')
  .attr('class', 'grid')
  .call(d3.axisBottom(xScale)
          .tickSize(innerHeight)
          .tickFormat(''))
  .selectAll('.tick line')
  .attr('stroke-opacity', 0.2)
  .attr('stroke-dasharray', '2,2');

// Reflected power
g.append('text')
  .attr('fill', '#000')
  .attr('transform', 'rotate(-90)')
  .attr('x', -innerHeight / 2)
  .attr('y', -margin.left + 20)
  .attr('text-anchor', 'middle')
  .attr('font-size', '20px')
  .text('Reflected field (E refl)');

g.append('text')
  .attr('fill', '#000')
  .attr('x', innerWidth / 2)
  .attr('y', innerHeight + margin.bottom )
  .attr('text-anchor', 'middle')
  .attr('font-size', '20px')
  .text('Phase Shift (φ)');


  
function calculateReflectivity(r1, r2, phi) {
    const num = new Complex(-1*r1, 0).add(new Complex(r2*Math.cos(2*phi), r2*Math.sin(2*phi)));
    const den = new Complex(1, 0).sub(new Complex(r1*r2*Math.cos(2*phi), r1*r2*Math.sin(2*phi)));
    return num.div(den).abs();
}
function calculateReflectivityPhase(r1, r2, phi) {
    const num = new Complex(-1*r1, 0).add(new Complex(r2*Math.cos(2*phi), r2*Math.sin(2*phi)));
    const den = new Complex(1, 0).sub(new Complex(r1*r2*Math.cos(2*phi), r1*r2*Math.sin(2*phi)));
    return num.div(den).arg();
}

function updateReflectivityPlot(r1, r2) {
  const data = d3.range(-Math.PI, Math.PI, 0.01)
    .map(phi => ({ x: phi, y: calculateReflectivity(r1, r2, phi) }));

  const phasedata = d3.range(-Math.PI, Math.PI, 0.01)
    .map(phi => ({ x: phi, y: calculateReflectivityPhase(r1, r2, phi) }));

  const line = d3.line()
    .x(d => xScale(d.x))
    .y(d => yScale(d.y));

  g.select('.line')
    .datum(data)
    .attr('d', line);
}

d3.selectAll('.slider')
  .on('input', function() {
    const r1 = +document.getElementById('r1-slider').value;
    const r2 = +document.getElementById('r2-slider').value;

    updateReflectivityPlot(r1, r2);

    d3.select('#r1-value').text(r1.toFixed(2));
    d3.select('#r2-value').text(r2.toFixed(2));
  });

updateReflectivityPlot(0.9, 0.9);

