// get canvas and context
var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");

// set initial frequency
var frequency = document.getElementById("frequency").value;

// define plot function
function plot() {
  // clear canvas
  context.clearRect(0, 0, canvas.width, canvas.height);

  // set line color and width
  context.strokeStyle = "blue";
  context.lineWidth = 2;

  // calculate values for sine wave
  var amplitude = 100;
  var frequency = document.getElementById("frequency").value;
  var phase = 0;
  var xValues = [];
  var yValues = [];
  for (var i = 0; i < canvas.width; i++) {
    var x = i;
    var y = amplitude * Math.sin((2 * Math.PI * frequency * x) / canvas.width + phase);
    xValues.push(x);
    yValues.push(y);
  }

  // plot the sine wave
  context.beginPath();
  context.moveTo(xValues[0], canvas.height / 2 + yValues[0]);
  for (var i = 1; i < xValues.length; i++) {
    context.lineTo(xValues[i], canvas.height / 2 + yValues[i]);
  }
  context.stroke();
}

// update plot on frequency change
document.getElementById("frequency").addEventListener("input", function() {
  frequency = document.getElementById("frequency").value;
  plot();
});

// plot the initial sine wave
plot();
